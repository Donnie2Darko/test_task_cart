$(initPage);

function initPage() {
    $(".clear-cart").on("click", clearCart);
    $base_modal = $('#modal_base');
}

function clearCart() {
    var $this = $(this);
    $.post('clear-cart/', {"id": $this.val()})
        .success(function (data) {
            $base_modal.find('.modal-body-text').text(data.message);
            $base_modal.modal('show');
            if(data.status) {
                $("button, #modal_base").on('click', function() {
                    $this.parent().empty();
                    window.location.href = "";
                });
            }
        })
        .error(function () {
            $base_modal.find('.modal-body-text').text('An error occurred during request. Please, try again.');
            $base_modal.modal('show');
        })
}