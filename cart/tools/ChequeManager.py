from goods.models import Goods

class ChequeManager():
    
    def __init__(self, cart):
        self._cart = cart

    def get_cheque_obj(self):
        cheque_obj = dict(cheque=list(), taxes=0, total=0)
        db_data = Goods.objects.filter(id__in=self._cart.keys())
        for item in db_data:
            item_ordered_amount = self._cart[str(item.id)]['ordered_amount']
            if not item.category.no_taxes:
                cheque_obj['taxes'] += item.price * Goods.REGULAR_TAX_AMOUNT
            if item.imported:
                cheque_obj['taxes'] += item.price * Goods.IMPORTED_TAX_AMOUNT

            item.ordered_price = item.price * item_ordered_amount
            cheque_obj['total'] += item.ordered_price
            item.ordered_amount = item_ordered_amount
            cheque_obj['cheque'].append(item)

        cheque_obj['taxes'] = round(cheque_obj['taxes'], 2)
        cheque_obj['total'] = round(cheque_obj['total'] + cheque_obj['taxes'], 2)
        return cheque_obj
