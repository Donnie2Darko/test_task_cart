from django.shortcuts import render
from django.views.decorators.http import require_GET, require_POST
from django.http import JsonResponse
from .tools.ChequeManager import ChequeManager


@require_GET
def main(request):
    cart = request.session.get('cart', dict())
    response_data = ChequeManager(cart).get_cheque_obj()
    response_data['navbar'] = ['goods', 'cart']
    return render(request, 'cart/main.html', response_data)


@require_POST
def clear_cart(request):
    item_id = request.POST.get('id')
    if item_id == "all":
        request.session["cart"] = dict()
        request.session.modified = True
        return JsonResponse({'status': True, 'message': 'Cart is clear'})

    if request.session["cart"].get(item_id):
        request.session["cart"].pop(item_id, None)
        request.session.modified = True
        return JsonResponse({'status': True, 'message': 'Item is deleted successfully'})

    return JsonResponse({'status': False, 'message': 'Item that you want to delete isn\'t in cart'})


@require_POST
def add_to_cart(request):
    post_data = request.POST
    try:
        id = int(post_data.get('id'))
        ordered_amount = int(post_data.get('ordered_amount'))
    except ValueError:
        return JsonResponse({'status': False, 'message': 'Invalid request'})

    if not request.session.get('cart'):
        request.session['cart'] = {}

    if not request.session['cart'].get(id):
        request.session['cart'][id] = {'ordered_amount': ordered_amount}
    else:
        request.session['cart'][id]['ordered_amount'] += ordered_amount

    request.session.modified = True
    return JsonResponse({'status': True, 'message': 'Your item successfully is added to cart'})