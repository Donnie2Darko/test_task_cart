from django.conf.urls import patterns, url
from . import views


urlpatterns = [
    url(r'^$', views.main, name='cart'),
    url(r'^clear-cart/$', views.clear_cart, name='clear-cart'),
    url(r'^add-to-cart/$', views.add_to_cart, name='add-to-cart'),
]