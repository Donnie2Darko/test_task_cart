import json
from django.http import HttpRequest
from django.test.client import Client
from django.test import TestCase
from .tools.ChequeManager import ChequeManager
from .views import add_to_cart, clear_cart


class TestCart(TestCase):

    fixtures = ["goods.json"]

    def setUp(self):
        self.client = Client()
        self._request = HttpRequest()
        self._cart = {
            '1': {'ordered_amount': 5},
            '3': {'ordered_amount': 10},
            '7': {'ordered_amount': 1}
        }

    def test_add_to_cart(self):
        response = self._add_to_cart().content
        self.assertTrue(json.loads(response.decode('unicode_escape'))["status"])

    def _add_to_cart(self):
        self.client.get('/cart/')
        self._request.session = self.client.session
        self._request.method = 'POST'
        self._request.POST['id'] = 1
        self._request.POST['ordered_amount'] = 5
        return add_to_cart(self._request)

    def test_delete_from_cart(self):
        self._add_to_cart()
        response = clear_cart(self._request).content
        self.assertTrue(json.loads(response.decode('unicode_escape'))["status"])

        self._add_to_cart()
        self._request.POST['id'] = 5
        response = clear_cart(self._request).content
        self.assertFalse(json.loads(response.decode('unicode_escape'))["status"])

        self._add_to_cart()
        self._request.POST['id'] = "all"
        response = clear_cart(self._request).content
        self.assertDictEqual(self._request.session["cart"], {})

    def test_cheque_manager(self):
        cheque_data = ChequeManager(cart=self._cart).get_cheque_obj()
        self.assertEqual(cheque_data['total'], 185.76)
        self.assertEqual(cheque_data['taxes'], 3.12)