from django.db import models


class Category(models.Model):
    id = models.AutoField(primary_key=True)
    category = models.CharField('category', max_length=200, blank=False, null=False)
    no_taxes = models.BooleanField('no taxes', default=False)


class Goods(models.Model):
    REGULAR_TAX_AMOUNT, IMPORTED_TAX_AMOUNT = 0.1, 0.05
    id = models.AutoField(primary_key=True)
    name = models.CharField('name', max_length=200, blank=False, null=False)
    price = models.FloatField('price', default=0, blank=False, null=False)
    imported = models.BooleanField('imported', default=False)
    amount = models.IntegerField('amount', default=0, blank=False, null=False)
    category = models.ForeignKey(Category, verbose_name="category")

    def __unicode__(self):
        return u"{0}: {1}".format(self.name, self.price)