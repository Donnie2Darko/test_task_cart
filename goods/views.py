from django.shortcuts import render
from django.views.decorators.http import require_GET
from goods.models import Goods


@require_GET
def main(request):
    response_data = dict()
    response_data['navbar'] = ['goods', 'cart']
    response_data['data'] = Goods.objects.all()
    return render(request, 'goods/main.html', response_data)