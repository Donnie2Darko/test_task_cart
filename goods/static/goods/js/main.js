$(initPage);

function initPage() {
    $(".add-to-cart").on('click', addGoodsToCart);
    $base_modal = $('#modal_base');
}

function addGoodsToCart() {
    var $this = $(this),
        amount = $this.parent().find('input.amount').val();
    if (amount == 0) return;
    $.post('/cart/add-to-cart/', {'id': $this.val(), 'ordered_amount': amount})
        .success(function (data) {
            $base_modal.find('.modal-body-text').text(data.message);
            $base_modal.modal('show');
        })
        .error(function () {
            $base_modal.find('.modal-body-text').text('An error occurred during request. Please, try again.');
            $base_modal.modal('show');
        })
}