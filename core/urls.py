from django.conf.urls import url
from django.contrib import admin
from django.conf.urls import include
from django.views.generic import RedirectView

urlpatterns = [
    url(r'^admin/', admin.site.urls),
    url(r'^goods/', include('goods.urls')),
    url(r'^cart/', include('cart.urls')),
    url(r'^$', RedirectView.as_view(url='goods')),
]
