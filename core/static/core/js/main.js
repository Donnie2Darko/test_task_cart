function setAjaxCSRF(csrftoken) {
    $.ajaxSetup({
            beforeSend: function(xhr) {
            xhr.setRequestHeader("X-CSRFToken",csrftoken);
        }
    });
}